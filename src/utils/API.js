import axios from 'axios';
import Cookie from 'js-cookie';

const token = Cookie.get('token');

const tokenString = 'Bearer' + ' ' + token;
export default axios.create({
	baseURL: process.env.REACT_APP_BASE_URL,
	responseType: 'json',
	headers: {
		Authorization: tokenString,
	},
});
