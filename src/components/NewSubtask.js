import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { toast } from 'react-toastify';
import API from '../utils/API.js';
const NewSubtask = (props) => {
	const [ modal, setModal ] = useState(false);
	const toggleModal = () => {
		setModal(!modal);
	};
	const newSubtaskButton = <Button onClick={toggleModal}>New Subtask</Button>;
	const handleSubmit = (e) => {
		e.preventDefault();
		const description = e.target.description.value;
		const id = props.id;
		const requestBody = {
			query: `
                mutation{
                    addSubTask(todoInput:{
                        todoId:"${id}"
                        description:"${description}"
                        
                    }) {
                        owner{email}
                        _id
                        description
                        label
                        createdOn
                        deadline
                    }
                }
            `,
		};
		API.post('/', requestBody).then((data) => {}).catch((error) => {
			toast.error('Something went wrong');
		});
		toggleModal();
	};

	const ModalComponent = (
		<Modal isOpen={modal} toggle={toggleModal}>
			<ModalHeader toggle={toggleModal}>Add New Subtask</ModalHeader>
			<ModalBody>
				<Form onSubmit={handleSubmit}>
					<FormGroup>
						<Label for='description'>Description : </Label>
						<Input type='text' name='description' id='description' placeholder='Enter description' />
					</FormGroup>
					<Button color='primary' type='submit'>
						Add subtask
					</Button>
					<Button color='danger' onClick={toggleModal}>
						Cancel
					</Button>
				</Form>
			</ModalBody>
		</Modal>
	);
	return [ newSubtaskButton, ModalComponent ];
};
export default NewSubtask;
