import React from 'react';
import { Button, Form, Grid, Header, Message, Segment } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import API from '../utils/API.js';
import { toast } from 'react-toastify';
import AuthContext from '../contexts/AuthContext.js';

class Login extends React.Component {
	state = { email: '', password: '', status: '' };
	static contextType = AuthContext;
	handleChange = async (e, { name, value }) => this.setState({ [name]: value });
	submitLoginForm = async (event) => {
		event.preventDefault();
		const { email, password } = this.state;

		const requestBody = {
			query: `
                query{
                    login(userInput:{email:"${email}" password:"${password}"})
                    {
                        token
						userId
						userEmail
                    }
                }
            `,
		};

		const userLoginData = await API.post('/', requestBody);
		if (userLoginData.data.errors && userLoginData.data.errors.length === 1) {
			this.setState({ status: 'error' });
			toast.error('Auth Failed!');
		} else {
			const { token, userId, userEmail } = userLoginData.data.data.login;

			if (token && userId) {
				this.context.login(token, userId, userEmail);
				toast.status('Signined in sucessfully');
			}
			window.reload();
		}
	};

	render() {
		const { email, password } = this.state;
		return (
			<Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
				<Grid.Column style={{ maxWidth: 450 }}>
					<Header as='h2' color='#1b1c1d' textAlign='center'>
						Log-in to your account
					</Header>
					<Form size='large' error>
						<Segment stacked>
							<Form.Input
								fluid
								icon='user'
								iconPosition='left'
								name='email'
								value={email}
								placeholder='E-mail address'
								onChange={this.handleChange}
							/>
							<Form.Input
								fluid
								icon='lock'
								name='password'
								value={password}
								iconPosition='left'
								placeholder='Password'
								type='password'
								onChange={this.handleChange}
							/>
							{this.state.status === 'error' && (
								<Message
									error
									header='Auth Failed'
									content='Please write correct emailId and password'
								/>
							)}
							<Button
								color='#1b1c1d'
								fluid
								size='large'
								onClick={(event) => {
									this.submitLoginForm(event);
								}}
							>
								Login
							</Button>
						</Segment>
					</Form>
					<Message>
						New to us? <a href='signup'>Sign Up</a>
					</Message>
				</Grid.Column>
			</Grid>
		);
	}
}

export default Login;
