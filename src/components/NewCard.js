import React, { useState } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Cookie from 'js-cookie';
import API from '../utils/API.js';

const userId = Cookie.get('userId');

const AddNewTask = () => {
	const [ modal, setModal ] = useState(false);

	const toggleModal = () => {
		setModal(!modal);
	};

	const submitNewTaskForm = async (event) => {
		event.preventDefault();
		const description = event.target.description.value;
		const deadline = event.target.deadline.value;
		const requestBody = {
			query: `
            mutation {
                CreateTodo(todoInput: { deadline: "${deadline}", description: "${description.toString()}", userId: "${userId}", createdOn: "${new Date()}"}) {
                  _id
                }
              }`,
		};

		try {
			await API.post('/', requestBody);
		} catch (error) {
			throw error;
		}
		toggleModal();
	};

	const ModalComponent = (
		<Modal isOpen={modal} toggle={toggleModal}>
			<ModalHeader toggle={toggleModal}>Add New task</ModalHeader>
			<ModalBody>
				<Form
					onSubmit={(event) => {
						submitNewTaskForm(event);
					}}
				>
					<FormGroup>
						<Label for='label'>Title : </Label>
						<Input type='text' name='label' id='label' placeholder='Enter task label' />
					</FormGroup>
					<FormGroup>
						<Label for='description'>Description : </Label>
						<Input type='text' name='description' id='description' placeholder='Enter task description' />
					</FormGroup>
					<FormGroup>
						<Label for='deadline'>Search byDeadline : </Label>
						<Input type='date' name='deadline' id='deadline' placeholder='Enter task deadline' />
					</FormGroup>
					<Button color='primary' type='submit'>
						Add task
					</Button>
					<Button color='danger' onClick={toggleModal}>
						Cancel
					</Button>
				</Form>
			</ModalBody>
		</Modal>
	);

	const component = (
		<React.Fragment>
			<Button onClick={toggleModal}>Add New Task</Button>
			{ModalComponent}
		</React.Fragment>
	);
	return component;
};
export default AddNewTask;
