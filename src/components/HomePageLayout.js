import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Button, Container, Grid, Header, Icon, Menu, Responsive, Segment, Visibility } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';

class DesktopContainer extends Component {
	state = {};

	hideFixedMenu = () => this.setState({ fixed: false });
	showFixedMenu = () => this.setState({ fixed: true });

	render() {
		const { children } = this.props;
		const { fixed } = this.state;

		return (
			<Responsive>
				<Visibility once={false} onBottomPassed={this.showFixedMenu} onBottomPassedReverse={this.hideFixedMenu}>
					<Segment inverted textAlign='center' style={{ minHeight: 700, padding: '1em 0em' }} vertical>
						<Menu
							fixed={fixed ? 'top' : null}
							inverted={!fixed}
							pointing={!fixed}
							secondary={!fixed}
							size='large'
						>
							<Container>
								<Menu.Item position='right'>
									<Button
										as='a'
										inverted={!fixed}
										onClick={() => {
											window.location.assign('/login');
										}}
									>
										Log in
									</Button>
									<Button
										as='a'
										inverted={!fixed}
										primary={fixed}
										style={{ marginLeft: '0.5em' }}
										onClick={() => {
											window.location.assign('/signup');
										}}
									>
										Sign Up
									</Button>
								</Menu.Item>
							</Container>
						</Menu>
						<Container text>
							<Header
								as='h1'
								content='Karona App'
								inverted
								style={{
									fontSize: '4em',
									fontWeight: 'normal',
									marginBottom: 0,
									marginTop: '3em',
								}}
							/>
							<Header
								as='h2'
								content='Hey! You have a task pending. Signup on Karona'
								inverted
								style={{
									fontSize: '1.7em',
									fontWeight: 'normal',
									marginTop: '1.5em',
								}}
							/>
							<Button
								primary
								size='huge'
								onClick={() => {
									window.location.assign('/signup');
								}}
							>
								Get Started
								<Icon name='right arrow' />
							</Button>
						</Container>
					</Segment>
				</Visibility>

				{children}
			</Responsive>
		);
	}
}

DesktopContainer.propTypes = {
	children: PropTypes.node,
};

const HomepageLayout = () => (
	<DesktopContainer>
		<Segment inverted vertical style={{ padding: '5em 0em' }}>
			<Container>
				<Grid divided inverted stackable>
					<Grid.Row>
						<Grid.Column width={3} />
						<Grid.Column width={3} />
						<Grid.Column width={7} />
					</Grid.Row>
				</Grid>
			</Container>
		</Segment>
	</DesktopContainer>
);

export default HomepageLayout;
