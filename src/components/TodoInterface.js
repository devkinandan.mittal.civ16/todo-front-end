import React, { useState, useEffect, useContext } from 'react';
import CardComponent from './Card';
import { Modal, ModalHeader, ModalBody, Spinner } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Cookie from 'js-cookie';
import styled from 'styled-components';
import { Icon } from 'react-icons-kit';
import { plus } from 'react-icons-kit/metrize/plus';
import { keyframes } from 'styled-components';
import { shake } from 'react-animations';
import Navbar from './Navbar';
import AuthContext from '../contexts/AuthContext';
import API from '../utils/API.js';
import Empty from '../images/Empty.png';
import { toast } from 'react-toastify';
const userId = Cookie.get('userId');
const bounceAnimation = keyframes`${shake}`;

const TodoInterface = (props) => {
	const [ cards, setCards ] = useState([]);
	const [ modal, setModal ] = useState(false);
	const [ loading, setLoading ] = useState(true);
	const toggleModal = () => {
		setModal(!modal);
	};

	const authContext = useContext(AuthContext);
	var { category, tags, labels, date, search } = props;
	const priorityTag = tags === 'All' ? [ 'High', 'Medium', 'Low' ] : [ tags ];
	const label = labels === 'All' ? [ 'Work', 'Personal', 'Others' ] : [ labels ];
	const deadline = date === 'All' ? true : date;

	useEffect(
		() => {
			const requestBody = {
				query: `
                query{
                    getUserById(userInput:{
                    _id:"${authContext.userId}"
                }) {
                    todo {
						_id
						owner{email}
                        description
						label
						status
                        createdOn
						deadline
						category
						priority
                        subTasks{
                            _id
							description
							assignedTo
							status
							priority
							
                        }
                      }
                    }
                }
            `,
			};

			API.post('/', requestBody).then(async (data) => {
				const userData = await data.data.data.getUserById;
				if (userData) {
					setCards(
						userData.todo.filter(
							(card) =>
								card.category === category &&
								priorityTag.includes(card.priority, 0) &&
								label.includes(card.label) &&
								(deadline === true ? deadline : card.deadline < deadline) &&
								card.description.toUpperCase().includes(search.toUpperCase()),
						),
					);
				}

				setLoading(false);
			});
		},
		[ props ],
	);

	const deleteTask = async () => {
		const requestBody = {
			query: `
                query{
                    getUserById(userInput:{
                    _id:"${userId}"
                }) {
                    todo {
						owner{email},
						 _id
						 status
                        description
                        label
                        createdOn
						deadline
						category
						subTasks{
                            _id
							description
							assignedTo
							status
							priority
							
                        }
                      }
                    }
                }
            `,
		};
		const data = await API.post('/', requestBody);
		const cardsArray = data.data.data.getUserById.todo;
		setCards(cardsArray.filter((card) => card.category === 'Interface'));
	};

	const permanentDelete = async () => {
		const requestBody = {
			query: `
                query{
                    getUserById(userInput:{
                    _id:"${userId}"
                }) {
                    todo {
						owner{email}
						 _id
						 status
                        description
                        label
                        createdOn
						deadline
						category
						subTasks{
                            _id
							description
							assignedTo
							status
							priority
							
                        }
                      }
                    }
                }
            `,
		};
		const data = await API.post('/', requestBody);
		const cardsArray = data.data.data.getUserById.todo;
		setCards(cardsArray.filter((card) => card.category === 'Archive'));
	};

	const restoreTask = async () => {
		const requestBody = {
			query: `
                query{
                    getUserById(userInput:{
                    _id:"${userId}"
                }) {
                    todo {
						owner{email}
						 _id
						 status
                        description
                        label
                        createdOn
						deadline
						category
						subTasks{
                            _id
							description
							assignedTo
							status
							priority
							
                        }
                      }
                    }
                }
            `,
		};
		const data = await API.post('/', requestBody);
		const cardsArray = data.data.data.getUserById.todo;
		setCards(cardsArray.filter((card) => card.category === 'Archive'));
	};

	const submitNewTaskForm = async (event) => {
		event.preventDefault();
		const description = event.target.description.value;
		const deadline = event.target.deadline.value;
		const priority = event.target.priority.value;
		const label = event.target.tags.value;

		if (description && deadline && priority && label) {
			const requestBody = {
				query: `
            mutation {
				CreateTodo(todoInput: {deadline: "${deadline}", description: "${description.toString()}", userId: "${userId}", 
				priority: "${priority}"
				label: "${label}"
				createdOn: "${new Date()}"}) {
				  _id
				  owner{email}
                  description
                  label
				  createdOn
				  priority
				  category
                  deadline
                  subTasks{
                    _id
					description
					assignedTo
					status
					priority
					}
                }
              }`,
			};
			const todoData = await API.post('/', requestBody);
			const newTodo = todoData.data.data.CreateTodo;
			const day = new Date(deadline).getDate();
			const month = new Date(deadline).getMonth();
			const year = new Date(deadline).getFullYear();
			newTodo.deadline = day + '-' + month + '-' + year;

			setCards([ ...cards, newTodo ]);
			toggleModal();
			toast.success('Task created successfully!');
		} else {
			toast.error('One or more feilds are missing!');
		}
	};

	const cardComponent = cards.map((card) => {
		return (
			<CardComponent
				card={card}
				onDelete={() => {
					deleteTask();
				}}
				onPermanentDelete={() => {
					permanentDelete();
				}}
				onRestore={() => {
					restoreTask();
				}}
			/>
		);
	}, this);

	if (loading) {
		return (
			<div>
				<StyledSpinner style={{ width: '3rem', height: '3rem' }}>
					<span>Verifying user...</span>
				</StyledSpinner>
			</div>
		);
	}
	return (
		<React.Fragment>
			<br /> <br />
			<br />
			<Modal isOpen={modal} toggle={toggleModal}>
				<ModalHeader toggle={toggleModal}>Add New task</ModalHeader>
				<ModalBody>
					<Form
						onSubmit={(event) => {
							submitNewTaskForm(event);
						}}
					>
						<FormGroup>
							<Label for='description'>Description : </Label>
							<Input
								type='text'
								name='description'
								id='description'
								placeholder='Enter task description, Keep it short and simple'
							/>
						</FormGroup>
						<FormGroup>
							<Input for='tags' type='select' name='tags' id='tags'>
								<option>Personal</option>
								<option>Work</option>
								<option>Others</option>
							</Input>
							<br />
							<FormGroup>
								Priority <br />
								<Input for='priority' type='select' name='select' id='priority'>
									<option>High</option>
									<option>Medium</option>
									<option>Low</option>
								</Input>
							</FormGroup>
						</FormGroup>
						<FormGroup>
							<Label for='deadline'>Deadline : </Label>
							<Input type='date' name='deadline' id='deadline' placeholder='Enter task deadline' />
						</FormGroup>
						<Button color='primary' type='submit'>
							Add task
						</Button>
						<Button color='danger' onClick={toggleModal}>
							Cancel
						</Button>
					</Form>
				</ModalBody>
			</Modal>
			<Navbar />
			<br />
			<BouncyDiv>
				{cards.length === 0 ? <StyledImg src={Empty} alt='Good to go' /> : <div>{cardComponent}</div>}
			</BouncyDiv>
			<Loading>{loading === true ? <Spinner style={{ width: '3rem', height: '3rem' }} /> : null}</Loading>
			<StyledCursor>
				<StyledIcon icon={plus} size={50} onClick={toggleModal} disp={authContext.category} />
			</StyledCursor>
		</React.Fragment>
	);
};

const StyledIcon = styled(Icon)`
  position: fixed;
  color: #222;
  top: 90%;
  left: 95%;
  display: ${(props) => (props.disp === 'Archive' ? 'none' : 'block')} !important;
`;

const StyledImg = styled.img`
	position: absolute;
	width: 10%;
	left: 60%;
	top: 40%;
`;
const Loading = styled.div`
	position: absolute;
	left: 60%;
	top: 50%;
`;
const BouncyDiv = styled.div`
	animation: 1s ${bounceAnimation};
	postion: absolute;
	margin-left: 30% !important;
`;

const StyledSpinner = styled(Spinner)`
	position: absolute;
	top:45%;
	left:50%;
`;
export default TodoInterface;
const StyledCursor = styled.span`cursor: pointer;`;
