import React, { useState, useEffect, useContext } from 'react';
import {
	Button,
	Modal,
	ModalHeader,
	ModalBody,
	ModalFooter,
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
} from 'reactstrap';
import { toast } from 'react-toastify';
import Avatar from 'react-avatar';
import { Icon } from 'react-icons-kit';
import { ic_library_add } from 'react-icons-kit/md/ic_library_add';
import { ic_delete } from 'react-icons-kit/md/ic_delete';
import { ic_share } from 'react-icons-kit/md/ic_share';
import { ic_done_all } from 'react-icons-kit/md/ic_done_all';
import { ic_restore } from 'react-icons-kit/md/ic_restore';

import styled from 'styled-components';
import API from '../utils/API';
import AuthContext from '../contexts/AuthContext';
import Cookie from 'js-cookie';

const token = Cookie.get('token');
const userEmail = Cookie.get('userEmail');

const CardComponent = (props) => {
	const [ modal, setModal ] = useState(false);
	const [ subtaskModal, setSubtaskModal ] = useState(false);
	const [ updateModal, setUpdateModal ] = useState(false);
	const [ sharedUserData, setSharedUserData ] = useState([]);
	const [ subtasks, setSubtasks ] = useState([]);
	const [ markCompleteModal, setMarkCompleteModal ] = useState(false);
	const [ subtaskStatus, setSubtaskStatus ] = useState(true);
	const [ subtaskDescription, setSubtaskDescription ] = useState(''); //updateSubtaskdesctiption
	const [ sharingEmail, setSharingEmail ] = useState('');
	const [ dropdownOpen, setDropdownOpen ] = useState(false);
	const [ dropdownAssign, setDropdownAssign ] = useState(null);
	const [ archivedelete, setArchivedelete ] = useState(false);
	const [ deletemodal, setDeletemodal ] = useState(false);
	const [ restoreTask, setRestoreTask ] = useState(false);
	const [ updateSubtaskData, setUpdateSubtaskData ] = useState({
		_id: '',
		priority: '',
		status: '',
		description: '',
		assignedTo: '',
	});

	const authContext = useContext(AuthContext);

	const { card, onDelete, onPermanentDelete, onRestore } = props;
	const { _id, label, priority } = card;

	useEffect(
		() => {
			setSubtasks(card.subTasks);
			const { _id } = card;
			const requestBody = {
				query: `
				query{getTodoById(todoInput:{_id:"${_id}" token:"${token}"}){sharedWith{userEmail}}}
			`,
			};
			API.post('/', requestBody)
				.then((data) => {
					var sharedWithUserData = data.data.data.getTodoById.sharedWith;
					setSharedUserData(sharedWithUserData);
				})
				.catch((error) => {
					toast.error('Something went wrong!');
				});
		},
		[ card ],
	);

	const markCompleteToogle = () => {
		setMarkCompleteModal(!markCompleteModal);
	};

	const restoreModalToogle = () => {
		setRestoreTask(!restoreTask);
	};
	const deleteModalToogle = () => {
		setDeletemodal(!deletemodal);
	};

	const toggle = async () => {
		setModal(!modal);
	};

	const subtaskToogle = () => {
		setSubtaskModal(!subtaskModal);
	};

	const temptoogle = () => {
		setUpdateModal(!updateModal);
	};

	const archiveDeleteToogle = () => {
		setArchivedelete(!archivedelete);
	};

	const updateModalToogle = (subtask, event) => {
		setUpdateSubtaskData(subtask);
		setUpdateModal(!updateModal);
	};

	const dropDownToogle = () => setDropdownOpen(!dropdownOpen);

	const addSharingEmail = (event) => {
		event.preventDefault();
		setSharingEmail(event.target.value);
	};

	const addsubtaskDescription = (event) => {
		setSubtaskDescription(event.target.value);
	};

	const markComplete = async () => {
		const status = 'Completed';

		const { _id } = card;

		const deleteRequestBody = {
			query: `mutation{archiveTodo(todoInput:{todoId:"${_id}" status:"${status}" }){_id}}`,
		};
		await API.post('/', deleteRequestBody);

		onDelete();
		markCompleteToogle();
		toast.success('Task has been moved under Archive');
	};

	const addSubtask = async () => {
		if (subtaskDescription) {
			const requestBody = {
				query: `mutation{
				  addSubTask(todoInput:{token:"${token}" todoId:"${_id}" description:"${subtaskDescription}" assignedTo:"${userEmail}"})
				  {
					_id 
					description
					assignedTo
					priority
					}
				}`,
			};
			const data = await API.post('/', requestBody);
			const { description, assignedTo, priority } = data.data.data.addSubTask;
			const newSubtask = {
				_id: data.data.data.addSubTask._id,
				description: description,
				assignedTo: assignedTo,
				priority: priority,
				status: 'In Progress',
			};
			setSubtasks([ ...subtasks, newSubtask ]);
			setSubtaskDescription('');
			toast.success('Subtask Added');
		} else {
			toast.error('Please add the description');
		}

		subtaskToogle();
	};
	const updateSubtask = async (event) => {
		event.preventDefault();
		const status = subtaskStatus === true ? 'In Progress' : 'Completed';

		const requestBody = {
			query: `
			mutation{ updateTodo(todoInput:{ _id: "${_id}"
			subTasks:{
      			_id:"${updateSubtaskData._id}"
				description:"${subtaskDescription}"
				assignedTo: "${dropdownAssign ? dropdownAssign.split(' ')[1] : null}"
				status: "${status}"
    		}
  			}){
			subTasks{
				_id
				description
				assignedTo
				status
				priority
			}
  			}}
		`,
		};

		const data = await API.post('/', requestBody);
		const subTasks = data.data.data.updateTodo.subTasks;
		setDropdownAssign(null);
		setSubtasks(subTasks);
		temptoogle();
	};

	const addAssignment = (event) => {
		const name = event.currentTarget.textContent.toString();
		event.preventDefault();
		setDropdownAssign(event.currentTarget.textContent);
		toast.success('Subtask assigned to' + name);
	};

	const deleteArchive = async () => {
		const { _id } = card;

		const deleteRequestBody = {
			query: `mutation{removeTodo(todoInput:{todoId:"${_id}" }){result}}`,
		};
		await API.post('/', deleteRequestBody);
		toast.success('Task deleted Permanently');
		onPermanentDelete();
		archiveDeleteToogle();
	};

	const shareTodo = async () => {
		const email = sharingEmail;
		const requestBody = {
			query: `
			mutation{
  				addSharedUser(userInput:{email:"${email}" todoListId:"${_id}"}){sharedWith{userEmail}}
				}
		`,
		};

		const data = await API.post('/', requestBody);

		if (data.data.errors) {
			toast.error("User doesn't exist! Please provide a valid email adress");
		} else {
			setSharedUserData([ ...sharedUserData, { userEmail: email } ]);
			toast.success('Todo has been shared');
		}
	};

	const markSubtaskComplete = () => {
		setSubtaskStatus(!subtaskStatus);
	};

	const markCompletemodal = (
		<Modal isOpen={markCompleteModal} toggle={markCompleteToogle}>
			<ModalHeader>Mark {card.description} as Complete?</ModalHeader>
			<ModalFooter>
				<Button
					onClick={() => {
						markComplete();
					}}
				>
					Yes
				</Button>
				<Button onClick={markCompleteToogle}>No</Button>
			</ModalFooter>
		</Modal>
	);

	const deleteModal = (
		<Modal isOpen={deletemodal} toggle={deleteModalToogle}>
			<ModalHeader>Are you sure you want to delete {card.description}?</ModalHeader>
			<h6>Note: This will put the task into archive section</h6>
			<ModalFooter>
				<Button
					onClick={() => {
						deleteTodo();
					}}
				>
					{' '}
					Yes
				</Button>
				<Button onClick={deleteModalToogle}>No</Button>
			</ModalFooter>
		</Modal>
	);

	const deleteArchiveModal = (
		<Modal isOpen={archivedelete} toggle={archiveDeleteToogle}>
			<ModalHeader>Are you sure you want to delete {card.description}?</ModalHeader>
			<h6>Note: This will remove the task permanently.</h6>
			<ModalFooter>
				<Button
					onClick={() => {
						deleteArchive();
					}}
				>
					{' '}
					Yes
				</Button>
				<Button onClick={archiveDeleteToogle}>No</Button>
			</ModalFooter>
		</Modal>
	);
	const updateModalComponent = (
		<Modal isOpen={updateModal} toggle={temptoogle}>
			<ModalHeader>
				Update Subtask Hub<br />
				<input
					style={{ width: '239%' }}
					placeholder={updateSubtaskData.description}
					onChange={(event) => {
						addsubtaskDescription(event);
					}}
				/>
			</ModalHeader>

			<Dropdown isOpen={dropdownOpen} toggle={dropDownToogle} size='sm'>
				<DropdownToggle caret color='success'>
					Assign to Someone
				</DropdownToggle>
				<DropdownMenu>
					{sharedUserData.map((user) => {
						return (
							<DropdownItem
								onClick={(event) => {
									addAssignment(event);
								}}
							>
								<Avatar name={user.userEmail} size='30' textSizeRatio={1.75} round={true} />{' '}
								{user.userEmail}
							</DropdownItem>
						);
					})}
				</DropdownMenu>
			</Dropdown>
			<br />
			<span style={{ 'margin-left': '10px' }}>
				<input
					type='checkbox'
					onChange={() => markSubtaskComplete()}
					defaultChecked={updateSubtaskData.status === 'Completed'}
				/>{' '}
				Mark Complete
			</span>
			<br />
			<span style={{ 'margin-left': '10px' }}>
				<input type='checkbox' defaultChecked={updateSubtaskData.status === 'Completed'} /> High Priority
			</span>
			<span style={{ 'margin-left': '10px' }}>
				currently assigned to:{' '}
				{updateSubtaskData.assignedTo === 'null' ? userEmail : updateSubtaskData.assignedTo}
			</span>
			<br />
			<ModalFooter>
				<Button
					color='primary'
					onClick={(event) => {
						updateSubtask(event);
					}}
				>
					Update
				</Button>
				<Button color='secondary' onClick={temptoogle}>
					Cancel
				</Button>
			</ModalFooter>
		</Modal>
	);

	const deleteTodo = async () => {
		const status = 'In Progress';

		const { _id } = card;

		const deleteRequestBody = {
			query: `mutation{archiveTodo(todoInput:{todoId:"${_id}" status:"${status}" }){_id}}`,
		};
		await API.post('/', deleteRequestBody);

		onDelete();
		toast.success('Task moved to archive');
		deleteModalToogle();
	};

	const restore = async () => {
		const { _id } = card;
		const status = 'In Progress';
		const deleteRequestBody = {
			query: `mutation{unarchiveTodo(todoInput:{todoId:"${_id}" status:"${status}" }){_id}}`,
		};
		await API.post('/', deleteRequestBody);
		onRestore();
		restoreModalToogle();
	};

	const restoreTaskModal = (
		<Modal isOpen={restoreTask} toogle={restoreModalToogle}>
			<ModalHeader>Restore the task again on Interface?</ModalHeader>
			<ModalFooter>
				<Button
					onClick={() => {
						restore();
					}}
				>
					{' '}
					Restore
				</Button>
				<Button onClick={restoreModalToogle}>Cancel</Button>
			</ModalFooter>
		</Modal>
	);

	const modalComponent = (
		<Modal isOpen={modal} toggle={toggle}>
			<ModalHeader toggle={toggle}>Sharing Hub - {card.description.toUpperCase()}</ModalHeader>
			<ModalBody>
				{sharedUserData === null ? (
					<div>No shared User</div>
				) : (
					<StyledShareList>
						{sharedUserData.map((user) => {
							return (
								<React.Fragment>
									<Avatar name={user.userEmail} size='30' textSizeRatio={1.75} round={true} />{' '}
									{user.userEmail} <br />
									<hr />
								</React.Fragment>
							);
						})}
					</StyledShareList>
				)}
			</ModalBody>{' '}
			Share with someone: <br />
			<StyledInput
				placeholder='Please provide email here'
				onChange={(event) => {
					addSharingEmail(event);
				}}
			/>
			<ModalFooter>
				<Button
					color='primary'
					onClick={() => {
						shareTodo();
					}}
				>
					Share
				</Button>{' '}
				<Button color='secondary' onClick={toggle}>
					Cancel
				</Button>
			</ModalFooter>
		</Modal>
	);

	const subtaskModalComponent = (
		<Modal isOpen={subtaskModal} toggle={subtaskToogle}>
			<ModalHeader>Add Subtask</ModalHeader>
			<StyledInput
				type='input'
				placeholder='Description'
				onChange={(event) => {
					addsubtaskDescription(event);
				}}
			/>
			<br />
			<span>
				currently assigned to: {subtasks.assignedTo ? subtasks.assignedTo : userEmail}
				<br />By default it will be assigned to the creator. Later you can change it using update
			</span>
			<br />
			<ModalFooter>
				<Button
					color='primary'
					onClick={() => {
						addSubtask();
					}}
				>
					Add
				</Button>
				<Button color='secondary' onClick={subtaskToogle}>
					Cancel
				</Button>
			</ModalFooter>
		</Modal>
	);

	var { description, deadline } = card;

	return (
		<React.Fragment>
			{modalComponent}
			{subtaskModalComponent}
			{updateModalComponent}
			{markCompletemodal}
			{deleteArchiveModal}
			{restoreTaskModal}

			{deleteModal}
			<StyledCard>
				<StyledCardHeader>
					<span id='description'>{description}</span>
					<StyledOwner>
						<strong>{card.owner.email === userEmail ? 'Owner' : 'Shared By'}</strong> {card.owner.email}
					</StyledOwner>
				</StyledCardHeader>
				<StyledInfo>
					<StyledTags>
						<StyledLabel id='label' kind={label}>
							{label}
						</StyledLabel>
						<StyledPriority priority={priority}>
							{priority === 'High' ? 'High' : priority === 'Medium' ? 'Medium' : 'Low'}
						</StyledPriority>
					</StyledTags>

					<StyledDeadline>{deadline.split('T')[0]} </StyledDeadline>
					<StyledProgress>
						{authContext.category === 'Interface' ? (
							'In Progress'
						) : card.status === 'In Progress' ? (
							'Archived'
						) : (
							'Completed'
						)}
					</StyledProgress>

					<span />
					<StyledCursor>
						{authContext.category === 'Interface' && card.owner.email === userEmail ? (
							<StyledIcon icon={ic_share} size={22} onClick={toggle} />
						) : null}

						<span>
							{authContext.category === 'Interface' ? (
								<StyledIcon icon={ic_delete} size={22} onClick={deleteModalToogle} />
							) : (
								<StyledIcon icon={ic_delete} size={22} onClick={archiveDeleteToogle} />
							)}
						</span>
						<StyledMarkComplete>
							{authContext.category === 'Interface' ? card.owner.email === userEmail ? (
								<Icon icon={ic_done_all} size={22} onClick={markCompleteToogle} />
							) : null : card.status !== 'In Progress' ? null : (
								<Icon icon={ic_restore} size={22} onClick={restoreModalToogle} />
							)}
						</StyledMarkComplete>
					</StyledCursor>
				</StyledInfo>

				<StyledCardContent>
					{card.category === 'Interface' ? (
						<StyledCursor>
							<StyledSubTaskContainer border='dashed' onClick={subtaskToogle}>
								<span id='add'>
									<Icon icon={ic_library_add} size={24} /> Add Subtask
								</span>
							</StyledSubTaskContainer>
						</StyledCursor>
					) : null}

					{subtasks.map((subtask) => {
						return (
							<React.Fragment>
								<StyledSubTaskContainer border='solid'>
									<StyledSubtaskAvatar
										name={subtask.assignedTo === 'null' ? userEmail : subtask.assignedTo}
										size='30'
										textSizeRatio={1.75}
										round={true}
									/>
									<span id='description'>{subtask.description}</span>
									<StyledStatus>
										<span id='status' status={subtask.status}>
											{subtask.status}
										</span>
										<span>{subtask.priority === false ? '' : 'High Priority'}</span>
										<StyledCursor>
											<span
												id={subtask._id}
												class='update'
												onClick={(event) => {
													updateModalToogle(subtask, event);
												}}
											>
												Update
											</span>
										</StyledCursor>
									</StyledStatus>
									{}
								</StyledSubTaskContainer>
							</React.Fragment>
						);
					})}

					<StyledFooter />
				</StyledCardContent>
			</StyledCard>
			<br />
		</React.Fragment>
	);
};

const StyledCardHeader = styled.div`
	background-color: #c4c4c4;

	border-radius: 10px 10px 0px 0px;
`;

const StyledInfo = styled.div`
	text-align: right;
	background-color: #c4c4c4;
`;

const StyledOwner = styled.span`
	position: absolute;
	right: 10%;
	font-size: 12px;
	background-color: #d0db61;
	border-radius: 20px 20px 20px 20px;
	padding: 2px 2px 2px 2px;
`;
const StyledStatus = styled.div`
	text-align: right !important;

	span {
		margin-right: 6px;
	}
	#status {
		font-size: 12px;
		padding: 3px 7px 2px 7px;
		border-radius: 10px 10px 10px 10px;
	}
	#mark {
		font-size: 12px;
		background: #d0db61;
		padding: 3px 7px 2px 7px;
		border-radius: 10px 10px 10px 10px;
	}
	.update {
		color: white;
		font-size: 12px;
		background: #39a2de;
		padding: 3px 7px 2px 7px;
		border-radius: 10px 10px 10px 10px;
	}
`;
const StyledFooter = styled.div`padding-bottom: 50px;`;
const StyledMarkComplete = styled.span`margin-right: 15px;`;

const StyledSubtaskAvatar = styled(Avatar)`
  margin-left: 5px;
  margin-top: 10px;
`;

const StyledTags = styled.span`
	font-size: 12px;
	margin-right: 56%;
`;

const StyledLabel = styled.span`
	background: ${(props) => (props.kind === 'Personal' ? '#39A2DE' : props.kind === 'Work' ? '#EBB11B' : '#4287f5')};
	color: ${(props) => (props.kind === ('Personal' || 'Others') ? 'white' : 'black')};
	border-radius: 20px;
	padding: 5px 10px 5px 10px;
`;

const StyledSubTaskContainer = styled.div`
	background: #e5e5e5;
	border: 1px ${(props) => props.border} black;

	width: 85%;
	height: auto !important;
	margin-left: auto;
	margin-right: auto;
	margin-top: 3px;
	margin-bottom: 3px;
	border-radius: 20px;
	#add {
		margin-left: 10%;
	}
	#description {
		margin-left: 50px;
	}
`;

const StyledCardContent = styled.div`
	font-size: 17px;
	border-top: 2px solid black;
	background: #e4e4e4;
	height: auto;
	max-height: 380px;
	overflow: scroll;
`;

const StyledInput = styled.input`width: 23em !important;`;

const StyledCard = styled.div`
	font-size: 24px;
	border: 1px solid #858585;
	width: 90%;
	border-radius: 10px 10px 0px 0px;
	#description {
		margin-left: 20px;
		margin-bottom: 20px;
	}
`;

const StyledDeadline = styled.span`
	font-size: 12px !important;
	background: #d0db61;
	border-radius: 20px;
	padding: 5px 10px 5px 10px;
`;

const StyledPriority = styled.span`
	font-size: 12px !important;
	background: #d0db61;
	border-radius: 20px;
	padding: 5px 10px 5px 10px;
`;

const StyledProgress = styled.span`
	margin-right: 0% !important;
	font-size: 12px !important;
	width: 40%;
	background: #d0db61;
	border-radius: 20px;
	padding: 5px 10px 5px 10px;
`;

const StyledCursor = styled.span`cursor: pointer;`;
const StyledIcon = styled(Icon)`
  padding: 5px;
`;

const StyledShareList = styled.div`
	height: auto;
	max-height: 380px;
	overflow: scroll;
`;
export default CardComponent;
