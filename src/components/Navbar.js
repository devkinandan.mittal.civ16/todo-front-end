import React from 'react';
import styled from 'styled-components';
import { Button, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { Icon, Form } from 'semantic-ui-react';
import Cookie from 'js-cookie';
import { toast } from 'react-toastify';
import Avatar from 'react-avatar';

import AuthContext from '../contexts/AuthContext';

class Navbar extends React.Component {
	static contextType = AuthContext;

	state = { displayLogout: 'none', search: '', loading: false };

	applyArchive = () => {
		if (this.context.category === 'Interface') {
			this.context.changeCategory('Archive');
		} else {
			this.context.changeCategory('Interface');
		}
	};

	applyHigh = () => {
		if (this.context.tags !== 'High') {
			toast.success('Set display to  High Priority');
			this.context.changeTags('High');
		} else {
			toast.success('Set display to  All Priority');
			this.context.changeTags('All');
		}
	};
	applyLow = () => {
		if (this.context.tags !== 'Low') {
			toast.success('Set display to  Low Priority');
			this.context.changeTags('Low');
		} else {
			toast.success('Set display to  All Priority');
			this.context.changeTags('All');
		}
	};
	applyMedium = () => {
		if (this.context.tags !== 'Medium') {
			toast.success('Medium tag selected');
			this.context.changeTags('Medium');
		} else {
			toast.success('Medium tag removed');
			this.context.changeTags('All');
		}
	};

	applyWork = () => {
		if (this.context.label !== 'Work') {
			toast.success('Work tag selected');
			this.context.changeLabels('Work');
		} else {
			toast.success('Work tag removed');
			this.context.changeLabels('All');
		}
	};
	applyPersonal = () => {
		if (this.context.label !== 'Personal') {
			toast.success('Personal tag selected');
			this.context.changeLabels('Personal');
		} else {
			toast.success('Personal tag removed');
			this.context.changeLabels('All');
		}
	};
	applyOthers = () => {
		if (this.context.label !== 'Others') {
			toast.success('Others tag selected');
			this.context.changeLabels('Others');
		} else {
			toast.success('Others tag removed');
			this.context.changeLabels('All');
		}
	};

	changeLogoutDisplay = () => {
		if (this.state.displayLogout === 'none') {
			this.setState({
				displayLogout: 'block',
			});
		} else {
			this.setState({
				displayLogout: 'none',
			});
		}
	};

	applyDate = (event) => {
		event.preventDefault();
		toast.success('Selected Date Applied');
		this.context.changeDate(event.target.date.value);
	};

	userLogout = () => {
		this.context.logout();
		toast.success('Logged out successfully!');
	};

	removeDate = () => {
		this.context.changeDate('All');
	};

	changeSearchForm = (event) => {
		event.preventDefault();
		this.context.changeSearch(event.target.value);
	};

	render() {
		const userEmail = Cookie.get('userEmail');
		const { tags, label, category } = this.context;

		if (this.state.loading === true) {
			return <StyledSpinner />;
		}
		return (
			<React.Fragment>
				<StyledNavbar>
					<StyledKarona>
						<Icon name='check square outline'>Karona</Icon>{' '}
					</StyledKarona>
					<StyledForm
						onChange={(event) => {
							this.changeSearchForm(event);
						}}
					/>
					<StyledHover>
						<StyledAvatar
							name={userEmail}
							size='40'
							textSizeRatio={1.75}
							round={true}
							onClick={() => {
								this.changeLogoutDisplay();
							}}
						/>
					</StyledHover>

					<StyledHover>
						<StyledLogout display={this.state.displayLogout} onClick={() => this.userLogout()}>
							Logout?
						</StyledLogout>
					</StyledHover>
				</StyledNavbar>
				<StyledSideBar>
					<StyledTags bg='#C4C4C4'>
						{' '}
						<Icon name='tag' />
						Tags
					</StyledTags>
					<StyledHover>
						<StyledTags
							bg='#33A5FF'
							onClick={() => {
								this.applyWork();
							}}
						>
							<Icon name='briefcase' />
							Work
							<Icon name='map pin' style={{ display: `${label === 'Work' ? 'inline-block' : 'none'}` }} />
						</StyledTags>
					</StyledHover>
					<StyledHover>
						<StyledTags
							bg='#7ed957'
							onClick={() => {
								this.applyPersonal();
							}}
						>
							<Icon name='clipboard outline' />
							Personal
							<Icon
								name='map pin'
								style={{ display: `${label === 'Personal' ? 'inline-block' : 'none'}` }}
							/>
						</StyledTags>
					</StyledHover>
					<StyledHover>
						<StyledTags
							bg='#ffbd59'
							onClick={() => {
								this.applyOthers();
							}}
						>
							<Icon name='meh' />
							Other
							<Icon
								name='map pin'
								style={{ display: `${label === 'Others' ? 'inline-block' : 'none'}` }}
							/>
						</StyledTags>
					</StyledHover>

					<StyledTags bg='#C4C4C4'>
						{' '}
						<Icon name='calendar alternate outline' />
						Calender
					</StyledTags>
					<Form
						onSubmit={(event) => {
							this.applyDate(event);
						}}
					>
						<StyledFormGroup>
							<Label for='date'>
								Search By Deadline :<br />{' '}
								<h6>Note: It will return all the tasks before the provided deadline </h6>
							</Label>
							<Input type='date' name='date' id='date' placeholder='Enter task deadline' />
							<StyledButtons>
								<Button type='submit' size={10} style={{ background: '#5CFC3C' }}>
									<span style={{ color: 'black' }}>Apply </span>
								</Button>
								<Button
									size={20}
									onClick={() => {
										this.removeDate();
									}}
									style={{ background: '#F60C0C' }}
								>
									Remove
								</Button>
							</StyledButtons>
						</StyledFormGroup>
					</Form>

					<StyledTags bg='#C4C4C4'>Priority</StyledTags>
					<StyledHover>
						<StyledTags
							bg='#FF5733'
							onClick={() => {
								this.applyHigh();
							}}
						>
							<Icon name='star outline' />High
							<Icon name='map pin' style={{ display: `${tags === 'High' ? 'inline-block' : 'none'}` }} />
						</StyledTags>
						<StyledTags
							bg='#FFBD33'
							onClick={() => {
								this.applyMedium();
							}}
						>
							<Icon name='star half' />
							Medium
							<Icon
								name='map pin'
								style={{ display: `${tags === 'Medium' ? 'inline-block' : 'none'}` }}
							/>
						</StyledTags>

						<StyledTags
							bg='#33FF57'
							onClick={() => {
								this.applyLow();
							}}
						>
							<Icon name='star half outline' />
							Low
							<Icon name='map pin' style={{ display: `${tags === 'Low' ? 'inline-block' : 'none'}` }} />
						</StyledTags>
					</StyledHover>
					<br />
					<StyledHover>
						<StyledTags
							bg='#C4C4C4'
							onClick={() => {
								this.applyArchive();
							}}
							active={category === 'Archive'}
						>
							<Icon name='archive' />
							{this.context.category === 'Archive' ? 'Go to Interface' : 'Go to Archive'}
						</StyledTags>
					</StyledHover>
				</StyledSideBar>
			</React.Fragment>
		);
	}
}

const StyledAvatar = styled(Avatar)`
  postion: absolute;
  margin-left: 93.5%;
  margin-top: 0.5%;
`;
const StyledHover = styled.span`cursor: pointer;`;
const StyledTags = styled.div`
	font-size: 20px;
	padding: 10px;
	background-color: ${(props) => (props.active ? '#708090' : props.bg)};
`;

const StyledKarona = styled.div`
	position: absolute !important;
	width: 128px;
	height: 54px;
	left: 72px;
	top: 5px;
	font-family: Spartan;
	font-style: normal;
	font-weight: 300;
	font-size: 36px;
	line-height: 54px;
	/* identical to box height */
	color: #343434 !important;
`;

const StyledButtons = styled.span`margin-left: 50%;`;

const StyledLogout = styled.span`
	display: ${(props) => props.display};

	postion: absolute;
	margin-left: 93.5%;
	margin-top: 0.5%;
	background: #ededed;
	color: #222;
	border: 0.1em solid #222;
	padding-top: 5px;
	padding-bottom: 10px;
	padding-right: 30px;
	padding-left: 10px;
`;
const StyledSpinner = styled(Spinner)`
	position: absolute;
	top:45%;
	left:50%;
`;
const StyledSideBar = styled.div`
	position: fixed;
	width: 320px;
	height: 100%;
	left: 0px;
	top: 8%;
	color: #343434 !important;
	background: #dbdbdb;
	overflow-y: scroll !important;
`;

const StyledForm = styled.input`
	position: absolute;
	width: 60%;
	height: 42px;
	left: 369px;
	top: 11px;
	border: none;
	background: #dadada;
	padding-left: 30px;
`;

const StyledNavbar = styled.div`
	position: fixed;
	width: 100%;
	height: 61px;
	left: 0px;
	top: 0px;
	background: #c4c4c4 !important;
	color: white;
`;

const StyledFormGroup = styled(FormGroup)`
  padding: 0.5em;
`;
export default Navbar;
