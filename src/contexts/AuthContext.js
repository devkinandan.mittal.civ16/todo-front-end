import React from 'react';

export default React.createContext({
	token: null,
	userId: null,
	userEmail: null,
	login: (token, userId, email) => {},
	logout: () => {},
	changeCategory: (category) => {},
	changeTags: (tags) => {},
	changeSearch: (search) => {},
	changeLabels: (label) => {},
	changeDate: (date) => {},
	date: 'All',
	tags: 'All',
	label: 'All',
	category: 'Interface',
	search: '',
});
