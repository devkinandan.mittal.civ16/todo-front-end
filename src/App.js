import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Cookie from 'js-cookie';

import Signup from './components/Signup';
import Login from './components/Login';
import TodoInterface from './components/TodoInterface';
import AuthContext from './contexts/AuthContext.js';
import Card from './components/Card';
import HomePageLayout from './components/HomePageLayout';

class App extends React.Component {
	static contextType = AuthContext;
	state = {
		token: Cookie.get('token') ? Cookie.get('token') : null,
		userId: Cookie.get('userId') ? Cookie.get('userId') : null,
		userEmail: null,
		category: 'Interface',
		search: '',
		label: 'All',
		tags: 'All',
		date: 'All',
	};

	changeCategory = (category) => {
		this.setState({
			category: category,
		});
	};
	changeDate = (date) => {
		this.setState({
			date: date,
		});
	};

	changeTags = (tags) => {
		this.setState({
			tags: tags,
		});
	};

	changeLabels = (label) => {
		this.setState({
			label: label,
		});
	};

	login = (token, userId, userEmail) => {
		this.setState({
			token: token,
			userId: userId,
			userEmail: userEmail,
		});

		Cookie.set('token', token, { expires: 7 });
		Cookie.set('userId', userId, { expires: 7 });
		Cookie.set('userEmail', userEmail, { expires: 7 });
		window.location.reload();
	};

	logout = () => {
		this.setState({
			token: null,
			userId: null,
		});
		Cookie.remove('token');
		Cookie.remove('userId');
		Cookie.remove('userEmail');
	};
	changeSearch = (search) => {
		this.setState({ search: search });
	};

	render() {
		return (
			<React.Fragment>
				<AuthContext.Provider
					value={{
						token: this.state.token,
						userId: this.state.userId,
						userEmail: this.state.email,
						login: this.login,
						logout: this.logout,
						changeCategory: this.changeCategory,
						changeTags: this.changeTags,
						changeLabels: this.changeLabels,
						changeDate: this.changeDate,
						changeSearch: this.changeSearch,
						tags: this.state.tags,
						category: this.state.category,
						label: this.state.label,
						date: this.state.date,
					}}
				>
					<Router>
						<Switch>
							<Route path='/' exact>
								{this.state.token ? <Redirect from='/' to='/mytodo' /> : <HomePageLayout />}
							</Route>

							<Route path='/login' exact>
								{!this.state.token ? <Login /> : <Redirect from='/login' to='/mytodo' />}
							</Route>

							<Route path='/signup' exact>
								{!this.state.token ? <Signup /> : <Redirect from='/signup' to='/mytodo' />}
							</Route>

							<Route path='/mytodo' exact>
								{this.state.token ? (
									<TodoInterface
										category={this.state.category}
										tags={this.state.tags}
										labels={this.state.label}
										date={this.state.date}
										search={this.state.search}
									/>
								) : (
									<Redirect from='/mytodo' to='/login' />
								)}
							</Route>

							<Route path='/archive' exact>
								<div>This will be archive list</div>
							</Route>

							<Route path='/card'>
								<Card />
							</Route>
						</Switch>
					</Router>
				</AuthContext.Provider>
			</React.Fragment>
		);
	}
}

export default App;
